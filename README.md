# ics-ans-role-ethercat

Ansible role to install and configure the ethercat kernel modules.

The `ethercat_master_device` shall be set to the network interface to use.
If left empty, the kernel module will be installed but the service won't be started.

This variable is used to configure the `MASTER0_DEVICE`. 

For multiple EtherCat masters, the variable `ethercat_master_device_list` should be used. 

This will configure `MASTER0_DEVICE`, `MASTER1_DEVICE` and so on.
See role variables below for example usage.

`ethercat_driver` is set to `generic` by default. You can change it to `e1000e` for CentOS native driver.


## Role Variables

```yaml
# Name of the ethercat master device
# ethercat_master_device: eth1
ethercat_master_device: ""
ethercat_master_device_list:
  - eth0
  - eth1
# Valid driver: generic | e1000e
ethercat_driver: "generic"
# Ethernet driver modules to use for EtherCAT operation.
ethercat_device_modules: "{{ ethercat_driver }}"
ethercat_rpm_version: 1.5.2.ESS1-1
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ethercat
```

## License

BSD 2-clause
