import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_kernel_module_present(host):
    # module should be installed
    # it is not loaded on the docker
    cmd = host.run("find /lib/modules/ -name 'ec_master.ko'")
    assert "ec_master.ko" in cmd.stdout
    cmd = host.run("find /lib/modules/ -name 'ec_generic.ko'")
    assert "ec_generic.ko" in cmd.stdout
