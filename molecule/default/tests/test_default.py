import os
import pytest
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-ethercat-default')


@pytest.mark.parametrize("name", ["dkms", "ethercat"])
def test_dkms_and_ethercat_services_enabled_and_running(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled


def test_kernel_modules_loaded(host):
    cmd = host.run("/usr/sbin/lsmod")
    assert "ec_master" in cmd.stdout
    assert "ec_generic" in cmd.stdout


def test_kernel_rt(host):
    cmd = host.run("uname -a")
    assert "PREEMPT RT" in cmd.stdout
